#include "objects.h"
#include <string.h>

Object *IntegerObject::__eq__(const Object *o) const {
    if (val == ((IntegerObject*)o)->val)
        return TrueObject::alloc();
    else
        return FalseObject::alloc();
}
Object *IntegerObject::__ne__(const Object *o) const {
    if (val != ((IntegerObject*)o)->val)
        return TrueObject::alloc();
    else
        return FalseObject::alloc();
}
Object *IntegerObject::__lt__(const Object *o) const {
    if (val < ((IntegerObject*)o)->val)
        return TrueObject::alloc();
    else
        return FalseObject::alloc();
}
Object *IntegerObject::__le__(const Object *o) const {
    if (val <= ((IntegerObject*)o)->val)
        return TrueObject::alloc();
    else
        return FalseObject::alloc();
}
Object *IntegerObject::__gt__(const Object *o) const {
    if (val > ((IntegerObject*)o)->val)
        return TrueObject::alloc();
    else
        return FalseObject::alloc();
}
Object *IntegerObject::__ge__(const Object *o) const {
    if (val >= ((IntegerObject*)o)->val)
        return TrueObject::alloc();
    else
        return FalseObject::alloc();
}

Object *IntegerObject::__add__(const Object *o) const { return IntegerObject::alloc(val + ((IntegerObject*)o)->val); }
Object *IntegerObject::__sub__(const Object *o) const { return IntegerObject::alloc(val - ((IntegerObject*)o)->val); }
Object *IntegerObject::__mul__(const Object *o) const { return IntegerObject::alloc(val * ((IntegerObject*)o)->val); }
Object *IntegerObject::__truediv__(const Object *o) const { return IntegerObject::alloc(val / ((IntegerObject*)o)->val); }
Object *IntegerObject::__floordiv__(const Object *o) const { return IntegerObject::alloc(val / ((IntegerObject*)o)->val); }
Object *IntegerObject::__mod__(const Object *o) const { return IntegerObject::alloc(val % ((IntegerObject*)o)->val); }
Object *IntegerObject::__pow__(const Object *o) const { return IntegerObject::alloc(val* ((IntegerObject*)o)->val); } // fix
// bit operations
Object *IntegerObject::__lshift__(const Object *o) const { return IntegerObject::alloc(val << ((IntegerObject*)o)->val); }
Object *IntegerObject::__rshift__(const Object *o) const { return IntegerObject::alloc(val >> ((IntegerObject*)o)->val); }
Object *IntegerObject::__and__(const Object *o) const { return IntegerObject::alloc(val & ((IntegerObject*)o)->val); }
Object *IntegerObject::__or__(const Object *o) const { return IntegerObject::alloc(val | ((IntegerObject*)o)->val); }
Object *IntegerObject::__xor__(const Object *o) const { return IntegerObject::alloc(val ^ ((IntegerObject*)o)->val); }
// unary operations
void IntegerObject::__neg__() { val = -val; }
void IntegerObject::__pos__() { }
void IntegerObject::__abs__() { val = val < 0 ? -val : val; }
void IntegerObject::__inv__() { val = ~val; }


Object *FalseObject::__and__(const Object *o) const {
    if (o->type == OBJ_FALSE || o->type == OBJ_TRUE)
        return FalseObject::alloc();
    return IntegerObject::alloc(val & ((IntegerObject*)o)->val); 
}

Object *FalseObject::__or__(const Object *o) const {
    if (o->type == OBJ_FALSE)
        return FalseObject::alloc();
    if (o->type == OBJ_TRUE)
        return TrueObject::alloc();
    return IntegerObject::alloc(val & ((IntegerObject*)o)->val); 
}

Object *FalseObject::__xor__(const Object *o) const {
    if (o->type == OBJ_FALSE)
        return FalseObject::alloc();
    if (o->type == OBJ_TRUE)
        return TrueObject::alloc();
    return IntegerObject::alloc(val & ((IntegerObject*)o)->val); 
}

Object *TrueObject::__and__(const Object *o) const {
    if (o->type == OBJ_FALSE)
        return FalseObject::alloc();
    if (o->type == OBJ_TRUE)
        return TrueObject::alloc();
    return IntegerObject::alloc(val & ((IntegerObject*)o)->val); 
}

Object *TrueObject::__or__(const Object *o) const {
    if (o->type == OBJ_FALSE || o->type == OBJ_TRUE)
        return TrueObject::alloc();
    return IntegerObject::alloc(val & ((IntegerObject*)o)->val); 
}

Object *TrueObject::__xor__(const Object *o) const {
    if (o->type == OBJ_FALSE)
        return TrueObject::alloc();
    if (o->type == OBJ_TRUE)
        return FalseObject::alloc();
    return IntegerObject::alloc(val & ((IntegerObject*)o)->val); 
}

Object *RealObject::__eq__(const Object *o) const {
    if (val == ((RealObject*)o)->val)
        return TrueObject::alloc();
    else
        return FalseObject::alloc();
}
Object *RealObject::__ne__(const Object *o) const {
    if (val != ((RealObject*)o)->val)
        return TrueObject::alloc();
    else
        return FalseObject::alloc();
}
Object *RealObject::__lt__(const Object *o) const {
    if (val < ((RealObject*)o)->val)
        return TrueObject::alloc();
    else
        return FalseObject::alloc();
}
Object *RealObject::__le__(const Object *o) const {
    if (val <= ((RealObject*)o)->val)
        return TrueObject::alloc();
    else
        return FalseObject::alloc();
}
Object *RealObject::__gt__(const Object *o) const {
    if (val > ((RealObject*)o)->val)
        return TrueObject::alloc();
    else
        return FalseObject::alloc();
}
Object *RealObject::__ge__(const Object *o) const {
    if (val >= ((RealObject*)o)->val)
        return TrueObject::alloc();
    else
        return FalseObject::alloc();
}

Object *ListObject::__iter__() {
    return new ListIterator(this);
}

Object *DictObject::__iter__() {
    return new DictIterator(this);
}

hash_t StringObject::__hash__() const {
    if (hash == 0)  {
        const char *p = val;
        int len = size;

        hash_t x = *p << 7;
        while (len-- >= 0) {
            x= (1000003*x)^*p++;
        }
        x ^= size;
        if (x == -1)
            x = -2;
        const_cast<StringObject*>(this)->hash = x;
    }
    return hash;
}

Object *StringObject::__eq__(const Object *o) const {
    if (strcmp(val, ((StringObject*)o)->val) == 0)
        return TrueObject::alloc();
    else
        return FalseObject::alloc();
}
Object *StringObject::__ne__(const Object *o) const {
    if (strcmp(val, ((StringObject*)o)->val) != 0)
        return TrueObject::alloc();
    else
        return FalseObject::alloc();
}
Object *StringObject::__lt__(const Object *o) const {
    if (strcmp(val, ((StringObject*)o)->val) < 0)
        return TrueObject::alloc();
    else
        return FalseObject::alloc();
}
Object *StringObject::__le__(const Object *o) const {
    if (strcmp(val, ((StringObject*)o)->val) <= 0)
        return TrueObject::alloc();
    else
        return FalseObject::alloc();
}
Object *StringObject::__gt__(const Object *o) const {
    if (strcmp(val, ((StringObject*)o)->val) > 0)
        return TrueObject::alloc();
    else
        return FalseObject::alloc();
}
Object *StringObject::__ge__(const Object *o) const {
    if (strcmp(val, ((StringObject*)o)->val) >= 0)
        return TrueObject::alloc();
    else
        return FalseObject::alloc();
}

Object *StringObject::__contains__(const Object *key) const {
    const char *substr = ((StringObject*)key)->val;
    if (strstr(val, substr) != NULL)
        return TrueObject::alloc();
    else
        return FalseObject::alloc();
}
Object *StringObject::__getitem__(const Object *key) const {
    int i = ((IntegerObject*)key)->val;
    char *ch = new char[2];
    ch[0] = val[i];
    ch[1] = '\0';
    return StringObject::alloc(ch, true);
}
