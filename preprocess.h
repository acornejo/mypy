#include <map>
#include <vector>
#include <string>
#include <iostream>
#include "ast.h"
#include "builtins.h"
#include "types.h"

using namespace ast;
using namespace ast::node_type;

template<typename T, typename V>
void dfs_traverse(T *root, V *v) {
    std::vector<T*> stack;
    stack.push_back(root);

    while(!stack.empty()) {
        T *current = stack.back(), *next = current->firstChild;

        if (!v->enter(current)) {
            stack.pop_back();
            next = current->nextSibling;
        }

        while(next == NULL && !stack.empty()) {
            current = stack.back();
            next = current->nextSibling;
            v->leave(current);
            stack.pop_back();
        }

        if (next != NULL)
            stack.push_back(next);
    }
}


typedef std::map<real_t, NodeList> co_const_real_t;
typedef std::map<integer_t, NodeList> co_const_integer_t;
typedef std::map<std::string, NodeList> co_const_string_t;
typedef NodeList co_const_func_t;
typedef std::map<std::string, NodeList> co_varnames_t;

struct LocalContext {
    Node *body;
    LocalContext *parent;
    co_varnames_t co_inner;
    co_varnames_t co_outer;
};

class VarIndex {
    public:
        co_const_real_t co_const_real;
        co_const_integer_t co_const_integer;
        co_const_string_t co_const_string;
        co_const_func_t co_const_func;
        std::vector<LocalContext *> contexts;
        LocalContext *l;

        std::vector<ConstObject*> &consts;
        std::vector<BuiltinObject*> &builtins;
        Node &functions;

        VarIndex(Node *root, std::vector<ConstObject*> &_consts, std::vector<BuiltinObject*> &_builtins, Node &_functions): consts(_consts), builtins(_builtins), functions(_functions) {
            builtins_init();
            l = new LocalContext();
            l->parent = NULL;
            l->body = root;
            contexts.push_back(l);
            dfs_traverse(l->body, this);
            finish();
        }

        void add_innervar(Node *n, std::string *s) {
            co_varnames_t::iterator it = l->co_inner.find(*s);
            if (it != l->co_inner.end()) {
                it->second.push_back(n);
            } else {
                NodeList list;
                list.push_back(n);
                it = l->co_inner.insert(it, std::make_pair(*s, list));
            }

            co_varnames_t::iterator oit = l->co_outer.find(*s);
            if (oit != l->co_outer.end()) {
                it->second.insert(it->second.end(), oit->second.begin(), oit->second.end());
                oit->second.clear();
                l->co_outer.erase(oit);
            }
        }

        void add_outervar(Node *n, std::string *s) {
            co_varnames_t::iterator it = l->co_inner.find(*s);
            if (it != l->co_inner.end()) {
                it->second.push_back(n);
            } else {
                co_varnames_t::iterator oit = l->co_outer.find(*s);
                if (oit != l->co_outer.end()) {
                    oit->second.push_back(n);
                } else {
                    NodeList list;
                    list.push_back(n);
                    l->co_outer[*s] = list;
                }
            }
        }

        bool enter(Node *n) {
            if (n->type == AST_VARIABLE) {
                Variable *v = (Variable*)n;
                if (v->write)
                    add_innervar(v, v->val);
                else
                    add_outervar(v, v->val);
            } else if (n->type == AST_INTEGER) {
                Integer *v = (Integer*)n;
                co_const_integer_t::iterator it = co_const_integer.find(v->val);
                if (it != co_const_integer.end()) {
                    it->second.push_back(n);
                } else {
                    NodeList list;
                    list.push_back(n);
                    co_const_integer[v->val] = list;
                }
            } else if (n->type == AST_REAL) {
                Real *v = (Real*)n;
                co_const_real_t::iterator it = co_const_real.find(v->val);
                if (it != co_const_real.end()) {
                    it->second.push_back(n);
                } else {
                    NodeList list;
                    list.push_back(n);
                    co_const_real[v->val] = list;
                }
            } else if (n->type == AST_STRING) {
                String *v = (String*)n;
                co_const_string_t::iterator it = co_const_string.find(*(v->val));
                if (it != co_const_string.end()) {
                    it->second.push_back(n);
                } else {
                    NodeList list;
                    list.push_back(n);
                    co_const_string[*(v->val)] = list;
                }
            } else if (n->type == AST_FUNCTION) {
                co_const_func.push_back(n);

                LocalContext *ctx = new LocalContext();
                ctx->body = n;
                ctx->parent = l;
                contexts.push_back(ctx);
                l = ctx;
            } else if (n->type == AST_IDENT_LIST) {
                Node *c = n->firstChild;
                while (c != NULL) {
                    add_innervar(c, ((Variable*)c)->val);
                    c = c->nextSibling;
                }
                return false;
            }
            return true;
        }

        void leave(Node *n) {
            if (n->type == AST_FUNCTION) {
                l = l->parent;
            }
        }

        void finish() {
            std::vector<LocalContext *>::iterator ctx;
            co_varnames_t::iterator vit;
            NodeList::iterator nit;
            int iidx, bidx, cidx = 0;

            std::map<std::string,int> builtins_idx;

            // Add integers to constant table
            for (co_const_integer_t::iterator i = co_const_integer.begin(); i != co_const_integer.end(); ++i, cidx++) {
                for (nit = i->second.begin(); nit != i->second.end(); ++nit)
                    ((Const*)(*nit))->idx = cidx;
                consts.push_back(new ConstObject(((Integer*)*(i->second.begin()))->val));
            }

            // Add reals to constant table
            for (co_const_real_t::iterator i = co_const_real.begin(); i != co_const_real.end(); ++i, cidx++) {
                for (nit = i->second.begin(); nit != i->second.end(); ++nit)
                    ((Const*)(*nit))->idx = cidx;
                consts.push_back(new ConstObject(((Real*)*(i->second.begin()))->val));
            }

            // Add strings to constant table
            for (co_const_string_t::iterator i = co_const_string.begin(); i != co_const_string.end(); ++i, cidx++) {
                for (nit = i->second.begin(); nit != i->second.end(); ++nit)
                    ((Const*)(*nit))->idx = cidx;
                consts.push_back(new ConstObject(*(((String*)*(i->second.begin()))->val)));
            }

            // Go through every context
            for (ctx = contexts.begin(); ctx != contexts.end(); ++ctx) {
                // Assign indices to local variables
                for (vit = (*ctx)->co_inner.begin(), iidx = 0; vit != (*ctx)->co_inner.end(); ++vit, iidx++) {
                    for (nit = vit->second.begin(); nit != vit->second.end(); ++nit) {
                        Variable *v = (Variable*)(*nit);
                        v->idx = iidx;
                        v->vtype = VAR_LOCAL;
                    }
                }

                // Search for outer variables in builtins.
                for (vit = (*ctx)->co_outer.begin(); vit != (*ctx)->co_outer.end(); ++vit) {
                    std::string name(vit->first);
                    std::map<std::string, int>::iterator it = builtins_idx.find(name);
                    if (it == builtins_idx.end()) {
                        builtins_t::iterator bit = builtins_dict.find(name);
                        if (bit != builtins_dict.end()) {
                            it = builtins_idx.insert(it, std::make_pair(name, builtins.size()));
                            builtins.push_back(bit->second);
                        }
                    }
                    if (it != builtins_idx.end()) {
                        for (nit = vit->second.begin(); nit != vit->second.end(); ++nit) {
                            Variable *v = (Variable*)(*nit);
                            v->idx = it->second;
                            v->vtype = VAR_BUILTIN;
                        }
                    } else {
                        Variable *fcur = new Variable(new std::string(name));
                        Variable *cur = fcur;

                        LocalContext *parent = (*ctx)->parent;
                        while (parent != NULL) {
                            co_varnames_t::iterator it = parent->co_inner.find(name);
                            if (it != parent->co_inner.end()) {
                                Variable *v = (Variable*)(it->second.front());
                                cur->idx = v->idx;
                                cur->vtype = VAR_LOCAL;
                                break;
                            } else if (parent->parent != NULL) {
                                FunctionDef *f = (FunctionDef*)parent->body;
                                Node *n = f->binds.firstChild;
                                while (n != NULL) {
                                    if (*(((Variable*)n)->val) == name) {
                                        cur->idx = bidx;
                                        cur->vtype = VAR_PARENT;
                                        break;
                                    }
                                    bidx++;
                                    n = n->nextSibling;
                                }
                                if (n == NULL) {
                                    cur->idx = bidx;
                                    cur->vtype = VAR_PARENT;
                                    cur = new Variable(new std::string(name));
                                    f->binds.appendChild(cur);
                                }
                            }
                            parent = parent->parent;
                        }

                        if (parent == NULL) {
                            std::cout << "NameError: " << name << " is not defined.\n";
                            delete fcur;
                        } else {
                            FunctionDef *func = (FunctionDef*)(*ctx)->body;
                            bidx = func->binds.num_children;
                            func->binds.appendChild(fcur);
                            for (nit = vit->second.begin(); nit != vit->second.end(); ++nit) {
                                Variable *v = (Variable*)(*nit);
                                v->idx = bidx;
                                v->vtype = VAR_PARENT;
                            }
                        }
                    }
                }
            }

            // Add functions to constant table
            for (ctx = contexts.begin(); ctx != contexts.end(); ++ctx) {
                if ((*ctx)->parent != NULL) {
                    FunctionDef *func = (FunctionDef*)(*ctx)->body;
                    func->idx = cidx++;
                    consts.push_back(new ConstObject(func->params->num_children, (*ctx)->co_inner.size(), func->binds.num_children));
                    functions.appendChild(func);
                }
                delete *ctx;
            }
            contexts.clear();
        }
};
