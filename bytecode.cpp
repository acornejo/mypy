#include "bytecode.h"
#include <string>
#include <string.h>
#include <vector>
#include <sstream>

static const char *opnames[] = {
    "POP_TOP",
    "DUP_TOP",
    "LOAD_NONE",
    "LOAD_TRUE",
    "LOAD_FALSE",
    "LOAD_SUBSCR",
    "DELETE_SUBSCR",
    "STORE_SUBSCR",
    "COMPARE_LT",
    "COMPARE_LE",
    "COMPARE_GT",
    "COMPARE_GE",
    "COMPARE_EQ",
    "COMPARE_NE",
    "COMPARE_IS",
    "COMPARE_IN",
    "BINARY_ADD",
    "BINARY_SUBTRACT",
    "BINARY_MULTIPLY",
    "BINARY_TRUE_DIVIDE",
    "BINARY_FLOOR_DIVIDE",
    "BINARY_MODULO",
    "BINARY_POW",
    "BINARY_LSHIFT",
    "BINARY_RSHIFT",
    "BINARY_AND",
    "BINARY_OR",
    "BINARY_XOR",
    "UNARY_POSITIVE",
    "UNARY_NEGATIVE",
    "UNARY_INVERT",
    "UNARY_NOT",
    "GET_ITER",
    "RETURN_VALUE",
    "LOAD_CONST", // const_index
    "LOAD_PARENT", // parent_index
    "LOAD_BUILTIN", // builtin_index
    "LOAD_LOCAL", // local_index
    "STORE_LOCAL", // local_index
    "DELETE_LOCAL", // local_index
    "CALL_FUNCTION", // num_params
    "BUILD_MAP", // num_params
    "BUILD_LIST", // num_params
    "BUILD_BINDING", // num_params
    "JUMP_ABSOLUTE", // address
    "POP_JUMP_IF_TRUE", // address
    "POP_JUMP_IF_FALSE", // address
    "JUMP_IF_TRUE_OR_POP", // address
    "JUMP_IF_FALSE_OR_POP", // address
    "FOR_ITER", // address
};


void Opcode::write(Buffer &buf) const {
    buf.write(&opcode, 1);
    if (opcode >= HAS_OPARG) {
        buf.write(&oparg, sizeof(oparg_t));
    }
}

void Opcode::read(Buffer &buf) {
    buf.read(&opcode, 1);
    if (opcode >= HAS_OPARG) {
        buf.read(&oparg, sizeof(oparg_t));
    }
}

std::string Opcode::str() const {
    std::stringstream ss;
    ss << opnames[opcode];
    if (opcode >= HAS_OPARG)
        ss << " " << oparg;
    return ss.str();
}

ConstObject::ConstObject(const real_t &real): type(CO_REAL) {
    val = new real_t(real);
}

ConstObject::ConstObject(const integer_t &integer): type(CO_INTEGER) {
    val = new integer_t(integer);
}

ConstObject::ConstObject(const std::string &str): type(CO_STRING) {
    int len = str.size()+1;
    val = new char[len];
    memcpy(val, str.c_str(), len);
}

ConstObject::ConstObject(const address_t &param, const address_t &vars, const address_t &bind): type(CO_FUNCTION) {
    val = new address_t[4];
    ((address_t*)val)[0] = 0;
    ((address_t*)val)[1] = param;
    ((address_t*)val)[2] = vars;
    ((address_t*)val)[3] = bind;
}

ConstObject::~ConstObject() {
    clear();
}

void ConstObject::clear() {
    if (val == NULL) return;
    switch(type) {
        case CO_REAL:
            delete ((real_t*)val);
            break;
        case CO_INTEGER:
            delete ((integer_t*)val);
            break;
        case CO_STRING:
            delete []((char*)val);
            break;
        case CO_FUNCTION:
            delete []((address_t*)val);
            break;
    }
    val = NULL;
}

void ConstObject::read(Buffer &b) {
    int len;
    char co_string[512], c;
    clear();
    b.read(&type, sizeof(type));
    switch(type) {
        case CO_REAL:
            val = new real_t();
            b.read(val, sizeof(real_t));
            break;
        case CO_INTEGER:
            val = new integer_t();
            b.read(val, sizeof(integer_t));
            break;
        case CO_STRING:
            len = 0;
            while (true) {
                b.read(&c, sizeof(c));
                co_string[len++] = c;
                if (c == '\0')
                    break;
            }
            val = new char[len];
            memcpy(val, co_string, len);
            break;
        case CO_FUNCTION:
            val = new address_t[4];
            b.read(val, sizeof(address_t)*4);
            break;
        default:
            break;
    }
}

void ConstObject::write(Buffer &b) const {
    b.write(&type, sizeof(type));
    switch(type) {
        case CO_REAL:
            b.write(val, sizeof(real_t));
            break;
        case CO_INTEGER:
            b.write(val, sizeof(integer_t));
            break;
        case CO_STRING:
            b.write(val, strlen((char*)val)+1);
            break;
        case CO_FUNCTION:
            b.write(val, sizeof(address_t)*4);
            break;
        default:
            break;
    }
}

std::string ConstObject::str() const {
    std::stringstream ss;
    address_t *a;
    switch(type) {
        case CO_REAL:
            ss << "[real] " << *((real_t*)val);
            break;
        case CO_INTEGER:
            ss << "[integer] " << *((integer_t*)val);
            break;
        case CO_STRING:
            ss << "[string] " << ((char*)val);
            break;
        case CO_FUNCTION:
            a=(address_t*)val;
            ss << "[func]@" << a[0] << " params:" << a[1] << " vars:" << a[2] << " binds:" << a[3];
            break;
        default:
            break;
    }
    return ss.str();
}
