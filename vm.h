#ifndef __VM_H__
#define __VM_H__

#include "objects.h"
#include "bytecode.h"
#include "compile.h"

#define MAX_VAR_STACK 128
#define MAX_VAL_STACK 128

class VM {
    private:
        Object *var_stack[MAX_VAR_STACK];
        Object *val_stack[MAX_VAL_STACK];
        Object **var_stack_p;
        Object **val_stack_p;
        Object **bind_stack_p;
        CompiledProgram &p;
        Opcode op;

    public:
        VM(CompiledProgram &);
        virtual ~VM();

        void set_pc(const address_t &);
        void read_opcode();
        void step();
        void reset();
};

#endif//__VM_H__
