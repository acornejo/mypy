#ifndef __BYTECODE_H__
#define __BYTECODE_H__

#include "types.h"
#include "buffer.h"
#include <string>
#include <vector>

typedef enum {
    POP_TOP,
    DUP_TOP,
    LOAD_NONE,
    LOAD_TRUE,
    LOAD_FALSE,
    LOAD_SUBSCR,
    DELETE_SUBSCR,
    STORE_SUBSCR,
    COMPARE_LT,
    COMPARE_LE,
    COMPARE_GT,
    COMPARE_GE,
    COMPARE_EQ,
    COMPARE_NE,
    COMPARE_IS,
    COMPARE_IN,
    BINARY_ADD,
    BINARY_SUBTRACT,
    BINARY_MULTIPLY,
    BINARY_TRUE_DIVIDE,
    BINARY_FLOOR_DIVIDE,
    BINARY_MODULO,
    BINARY_POW,
    BINARY_LSHIFT,
    BINARY_RSHIFT,
    BINARY_AND,
    BINARY_OR,
    BINARY_XOR,
    UNARY_POSITIVE,
    UNARY_NEGATIVE,
    UNARY_INVERT,
    UNARY_NOT,
    GET_ITER,
    RETURN_VALUE,
    HAS_OPARG,
    LOAD_CONST = HAS_OPARG, // const index
    LOAD_PARENT,   // parent index
    LOAD_BUILTIN,  // builtin index
    LOAD_LOCAL,    // local index
    STORE_LOCAL,   // local index
    DELETE_LOCAL,  // local index
    CALL_FUNCTION, // num params
    BUILD_MAP,     // num params
    BUILD_LIST,    // num params
    BUILD_BINDING, // num params
    JUMP_ABSOLUTE, // address
    POP_JUMP_IF_TRUE, // address
    POP_JUMP_IF_FALSE, // address
    JUMP_IF_TRUE_OR_POP, // address
    JUMP_IF_FALSE_OR_POP, // address
    FOR_ITER,      // address
} opcode_t;

typedef address_t oparg_t;

class Opcode {
    public:
        opcode_t opcode;
        oparg_t oparg;

        Opcode(opcode_t _opcode=LOAD_NONE, oparg_t _oparg=0): opcode(_opcode), oparg(_oparg) { }

        void write(Buffer &buf) const;
        void read(Buffer &buf);
        std::string str() const;
};

typedef enum {
    CO_INTEGER,
    CO_REAL,
    CO_STRING,
    CO_FUNCTION
} co_type_t;

class ConstObject {
    public:
        uint8_t type;
        void *val;

        ConstObject(): val(NULL) { }
        ConstObject(const real_t &);
        ConstObject(const integer_t &);
        ConstObject(const std::string &);
        ConstObject(const address_t &, const address_t &, const address_t &);
        ~ConstObject();
        void write(Buffer &b) const;
        void read(Buffer &b);
        void clear();
        std::string str() const;
};

#endif//__BYTECODE_H__
