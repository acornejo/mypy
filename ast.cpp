#include "ast.h"
#include "bytecode.h"
#include "parser.hpp"

namespace ast {

Node::~Node() {
    Node *n = firstChild;
    while (n != NULL) {
        Node *next = n->nextSibling;
        delete n;
        n = next;
    }
    childNodes.clear();
}

void Node::appendChild(Node *n) {
    if (firstChild != NULL) {
        lastChild->nextSibling = n;
    } else {
        firstChild = n;
    }
    lastChild = n;
    n->parentNode = this;
    n->nextSibling = NULL;
    num_children++;
    childNodes.push_back(n);
}

// void Node::prependChild(Node *n) {
//     childNodes.insert(childNodes.begin(), n);
//     n->nextSibling = firstChild;
//     n->parentNode = this;
//     firstChild = n;
// }

void Node::extend(Node *n) {
    Node *c = n->firstChild;
    while (c != NULL) {
        Node *next = c->nextSibling;
        appendChild(c);
        c = next;
    }
    n->firstChild = NULL;
    n->num_children = 0;
    delete n;
}

void Node::compileTo(Buffer &b) {
    Node *n = firstChild;
    while (n != NULL) {
        n->compileTo(b);
        n = n->nextSibling;
    }
}

void None::compileTo(Buffer &b) {
    Opcode(LOAD_NONE).write(b);
}

void True::compileTo(Buffer &b) {
    Opcode(LOAD_TRUE).write(b);
}

void False::compileTo(Buffer &b) {
    Opcode(LOAD_FALSE).write(b);
}

void Integer::compileTo(Buffer &b) {
    Opcode(LOAD_CONST, idx).write(b);
}

void Real::compileTo(Buffer &b) {
    Opcode(LOAD_CONST, idx).write(b);
}

void String::compileTo(Buffer &b) {
    Opcode(LOAD_CONST, idx).write(b);
}

void Variable::compileTo(Buffer &b) {
    switch(vtype) {
        case VAR_LOCAL:
            Opcode(LOAD_LOCAL, idx).write(b);
            break;
        case VAR_PARENT:
            Opcode(LOAD_PARENT, idx).write(b);
            break;
        case VAR_BUILTIN:
            Opcode(LOAD_BUILTIN, idx).write(b);
            break;
    }
}

void Binary::compileTo(Buffer &b) {
    left->compileTo(b);
    right->compileTo(b);
    switch(type) {
        case CMP_LT:
            Opcode(COMPARE_LT).write(b);
            break;
        case CMP_LE:
            Opcode(COMPARE_LE).write(b);
            break;
        case CMP_GT:
            Opcode(COMPARE_GT).write(b);
            break;
        case CMP_GE:
            Opcode(COMPARE_GE).write(b);
            break;
        case CMP_EQ:
            Opcode(COMPARE_EQ).write(b);
            break;
        case CMP_NE:
            Opcode(COMPARE_NE).write(b);
            break;
        case IN:
            Opcode(COMPARE_IN).write(b);
            break;
        case IS:
            Opcode(COMPARE_IS).write(b);
            break;
        case OP_ADD:
            Opcode(BINARY_ADD).write(b);
            break;
        case OP_SUB:
            Opcode(BINARY_SUBTRACT).write(b);
            break;
        case OP_MUL:
            Opcode(BINARY_MULTIPLY).write(b);
            break;
        case OP_DIV:
            Opcode(BINARY_TRUE_DIVIDE).write(b);
            break;
        case OP_IDIV:
            Opcode(BINARY_FLOOR_DIVIDE).write(b);
            break;
        case OP_MOD:
            Opcode(BINARY_MODULO).write(b);
            break;
        case OP_POW:
            Opcode(BINARY_POW).write(b);
            break;
        case BIT_LSHIFT:
            Opcode(BINARY_LSHIFT).write(b);
            break;
        case BIT_RSHIFT:
            Opcode(BINARY_RSHIFT).write(b);
            break;
        case BIT_AND:
            Opcode(BINARY_AND).write(b);
            break;
        case BIT_OR:
            Opcode(BINARY_OR).write(b);
            break;
        case BIT_XOR:
            Opcode(BINARY_XOR).write(b);
            break;
        // case AND:
        //     Opcode(BOOL_AND).write(b);
        //     break;
        // case OR:
        //     Opcode(BOOL_OR).write(b);
        //     break;
        default:
            break;
    }
}

void Unary::compileTo(Buffer &b) {
    down->compileTo(b);
    switch(type) {
        case OP_ADD:
            Opcode(UNARY_POSITIVE).write(b);
            break;
        case OP_SUB:
            Opcode(UNARY_NEGATIVE).write(b);
            break;
        case NOT:
            Opcode(UNARY_NOT).write(b);
            break;
        case BIT_NEG:
            Opcode(UNARY_INVERT).write(b);
            break;
        default:
            break;
    }
}

void And::compileTo(Buffer &b) {
    left->compileTo(b);
    address_t jmp_offset = b.get_offset()+1;
    Opcode(JUMP_IF_FALSE_OR_POP).write(b);
    right->compileTo(b);
    address_t jmp_address = b.get_offset();
    b.write(&jmp_address, sizeof(address_t), jmp_offset);
}

void Or::compileTo(Buffer &b) {
    left->compileTo(b);
    address_t jmp_offset = b.get_offset()+1;
    Opcode(JUMP_IF_TRUE_OR_POP).write(b);
    right->compileTo(b);
    address_t jmp_address = b.get_offset();
    b.write(&jmp_address, sizeof(address_t), jmp_offset);
}

void Pair::compileTo(Buffer &b) {
    left->compileTo(b);
    right->compileTo(b);
}

void Dict::compileTo(Buffer &b) {
    list->compileTo(b);
    Opcode(BUILD_MAP, list->num_children).write(b);
}

void List::compileTo(Buffer &b) {
    list->compileTo(b);
    Opcode(BUILD_LIST, list->num_children).write(b);
}

void Call::compileTo(Buffer &b) {
    params->compileTo(b);
    parent->compileTo(b);
    Opcode(CALL_FUNCTION, params->num_children).write(b);
}

void Subscript::compileTo(Buffer &b) {
    var->compileTo(b);
    idx->compileTo(b);
    Opcode(LOAD_SUBSCR).write(b);
}

void ExprStatement::compileTo(Buffer &b) {
    e->compileTo(b);
    Opcode(POP_TOP).write(b);
}

void Assign::compileTo(Buffer &b) {
    rval->compileTo(b);
    for (int i = 0; i<dups; i++)
        Opcode(DUP_TOP).write(b);
    if (lval->type == AST_VARIABLE)
        Opcode(STORE_LOCAL, ((Variable*)lval)->idx).write(b);
    else {
        ((Subscript*)lval)->var->compileTo(b);
        ((Subscript*)lval)->idx->compileTo(b);
        Opcode(STORE_SUBSCR).write(b);
    }
}

void Delete::compileTo(Buffer &b) {
    Node *n = list->firstChild;
    while (n != NULL) {
        if (n->type == AST_VARIABLE) {
            Opcode(DELETE_LOCAL, ((Variable*)n)->idx).write(b);
        } else {
            ((Subscript*)n)->var->compileTo(b);
            ((Subscript*)n)->idx->compileTo(b);
            Opcode(DELETE_SUBSCR).write(b);
        }

        n = n->nextSibling;
    }
}

void IfElse::compileTo(Buffer &b) {
    expr->compileTo(b);
    address_t elif_offset = b.get_offset()+1;
    Opcode(POP_JUMP_IF_FALSE).write(b);
    body->compileTo(b);
    if (ifelse) {
        address_t end_offset = b.get_offset()+1;
        Opcode(JUMP_ABSOLUTE).write(b);
        address_t elif_addr = b.get_offset();
        b.write(&elif_addr, sizeof(address_t), elif_offset);
        ifelse->compileTo(b);
        address_t end_addr = b.get_offset();
        b.write(&end_addr, sizeof(address_t), end_offset);
    } else {
        address_t elif_addr = b.get_offset();
        b.write(&elif_addr, sizeof(address_t), elif_offset);
    }
}

void For::compileTo(Buffer &b) {
    iterable->compileTo(b);
    Opcode(GET_ITER).write(b);
    start_loop = b.get_offset();
    end_loop.push_back(start_loop+1);
    Opcode(FOR_ITER).write(b);
    Opcode(STORE_LOCAL, ((Variable*)var)->idx).write(b);
    body->compileTo(b);
    Opcode(JUMP_ABSOLUTE, start_loop).write(b);

    address_t end_addr = b.get_offset();
    for(std::vector<address_t>::iterator i=end_loop.begin(); i != end_loop.end(); ++i)
        b.write(&end_addr, sizeof(address_t), *i);
}

void While::compileTo(Buffer &b) {
    start_loop = b.get_offset();
    expr->compileTo(b);
    end_loop.push_back(b.get_offset()+1);
    Opcode(POP_JUMP_IF_FALSE).write(b);
    body->compileTo(b);
    Opcode(JUMP_ABSOLUTE, start_loop).write(b);

    address_t end_addr = b.get_offset();
    for(std::vector<address_t>::iterator i=end_loop.begin(); i != end_loop.end(); ++i)
        b.write(&end_addr, sizeof(address_t), *i);
}

void Break::compileTo(Buffer &b) {
    Node *p = parentNode;
    while (p != NULL) {
        if (p->type == AST_FOR || p->type == AST_WHILE) {
            break;
        } else p = p->parentNode;
    }
    // FIX:
    // if (p == NULL)
    //     yyerror("SyntaxError: 'break' outside loop");
    ((Loop*)p)->end_loop.push_back(b.get_offset()+1);
    Opcode(JUMP_ABSOLUTE).write(b);
}

void Continue::compileTo(Buffer &b) {
    Node *p = parentNode;
    while (p != NULL) {
        if (p->type == AST_FOR || p->type == AST_WHILE) {
            break;
        } else p = p->parentNode;
    }

    // FIX:
    // if (p == NULL)
    //     yyerror("SyntaxError: 'continue' outside loop")

    Opcode(JUMP_ABSOLUTE, ((Loop*)p)->start_loop).write(b);
}

void IdentList::compileTo(Buffer &b) {
}

void FunctionDef::compileTo(Buffer &b) {
    if (binds.firstChild != NULL) {
        binds.compileTo(b);
        Opcode(LOAD_CONST, idx).write(b);
        Opcode(BUILD_BINDING, binds.num_children).write(b);
    } else
        Opcode(LOAD_CONST, idx).write(b);
}

void Return::compileTo(Buffer &b) {
    Node *p = parentNode;
    while (p != NULL) {
        if (p->type == AST_FUNCTION)
            break;
        else p = p->parentNode;
    }

    // FIX:
    // if (p != NULL)
    //     yyerror("SyntaxError: 'return' outside function")

    e->compileTo(b);
    Opcode(RETURN_VALUE).write(b);
}

}
