#ifndef __TYPES_H__
#define __TYPES_H__

#include <stdint.h>

typedef double real_t;
typedef long long integer_t;
typedef uint16_t address_t;
typedef long long hash_t;

#endif//__TYPES_H__
