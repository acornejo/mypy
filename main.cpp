#include <iostream>
#include "ast.h"
#include "parser.hpp"
#include "lexer.h"
#include "bytecode.h"
#include "compile.h"
#include "vm.h"

extern int yyparse(void *);

ast::Node *program;

void yyerror(void *scanner, const char *err) {
    std::cout << err << "\n";
}

int main(int argc, char *argv[]) {
    yyscan_t lexer_buffer;
    yylex_init(&lexer_buffer);
    yyparse(lexer_buffer);
    yylex_destroy(lexer_buffer);

    CompiledProgram prog;
    prog.compile(program);
    delete program;

    /* Read back program and print out. */
    prog.code.set_offset(0);
    std::cout << "Program is " << prog.code.get_size() << " bytes\n";

    std::cout << "Constants table." << std::endl;
    for (size_t i=0; i<prog.consts.size(); i++)
        std::cout << i << ": " << prog.consts[i]->str() << std::endl;
    std::cout << "Program opcodes." << std::endl;
    Opcode op;
    while (!prog.code.iseof()){
        size_t offset = prog.code.get_offset();
        op.read(prog.code);
        std::cout << offset << ": " << op.str() << std::endl;
    }

    prog.code.set_offset(0);
    VM vm(prog);
    while (true) {
        vm.step();
    }

    return 0;
}
