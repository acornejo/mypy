#ifndef __BUILTINS_H__
#define __BUILTINS_H__

#include <map>
#include <string>
#include "objects.h"

typedef std::map<std::string, BuiltinObject*> builtins_t;
extern builtins_t builtins_dict;
void builtins_init();

#endif
