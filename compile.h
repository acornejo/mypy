#ifndef __COMPILE_H__
#define __COMPILE_H__

#include "ast.h"
#include <vector>
#include "bytecode.h"
#include "objects.h"


class CompiledProgram {
    public:
        std::vector<ConstObject*> consts;
        std::vector<BuiltinObject*> builtins;
        ast::Node functions;
        Buffer header;
        Buffer code;

        CompiledProgram() {}
        void compile(ast::Node *main);
        virtual ~CompiledProgram() { functions.firstChild = NULL; }
};

#endif//__COMPILE_H__
