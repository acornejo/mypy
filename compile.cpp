#include "compile.h"
#include "preprocess.h"

void CompiledProgram::compile(Node *main) {
    VarIndex(main, consts, builtins, functions);

    // // write the constants table (sans function addresses)
    // int16_t num = consts.size();
    // b.write(&num, sizeof(num));
    // std::vector<address_t> addr_offset;
    // for (int i=0; i<num; i++) {
    //     if (consts[i]->type == CO_FUNCTION)
    //         addr_offset.push_back(b.get_offset()+1);
    //     consts[i]->write(b);
    // }

    // write the main code
    main->compileTo(code);
    Opcode(JUMP_ABSOLUTE, code.get_offset()).write(code);

    // write the body of every function
    std::vector<address_t> addr;
    Node *f = functions.firstChild;
    while (f != NULL) {
        addr.push_back(code.get_offset());
        ((FunctionDef*)f)->body->compileTo(code);
        Opcode(LOAD_NONE).write(code);
        Opcode(RETURN_VALUE).write(code);
        f = f->nextSibling;
    }

    // resolve the address of every function
    std::vector<address_t>::iterator addr_it = addr.begin();
    for (size_t i=0; i<consts.size(); i++) {
        if (consts[i]->type == CO_FUNCTION) {
            address_t a = *addr_it;
            ((address_t*)consts[i]->val)[0] = a;
            /* b.write(&a, sizeof(address_t), addr_offset[i]);  */
            addr_it++;
        }
    }
}
