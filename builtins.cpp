#include "builtins.h"
#include <iostream>

builtins_t builtins_dict;

class PrintBuiltin: public BuiltinObject {
    public:
        static BuiltinObject *alloc() {
            static PrintBuiltin p;
            return &p;
        }
        virtual Object *call(address_t num, Object **param) {
            for (int i=0; i<num; i++) {
                std::cout << param[i]->__str__();
                if (i+1 < num)
                    std::cout << " ";
            }
            std::cout << "\n";
            return NoneObject::alloc();
        }
};

class LenBuiltin: public BuiltinObject {
    public:
        static BuiltinObject *alloc() {
            static LenBuiltin o;
            return &o;
        }
        virtual Object *call(address_t num, Object **param) {
            return IntegerObject::alloc(param[0]->__len__());
        }
};

class NextBuiltin: public BuiltinObject {
    public:
        static BuiltinObject *alloc() {
            static NextBuiltin o;
            return &o;
        }
        virtual Object *call(address_t num, Object **param) {
            Object *n = param[0]->__next__();
            if (n == NULL && num > 1)
                return param[1];
            return n;
        }
};

class IterBuiltin: public BuiltinObject {
    public:
        static BuiltinObject *alloc() {
            static NextBuiltin o;
            return &o;
        }
        virtual Object *call(address_t num, Object **param) {
            if (num == 2) {

            }
            return param[0]->__iter__();
        }
};

class BoolBuiltin: public BuiltinObject {
    public:
        static BuiltinObject *alloc() {
            static BoolBuiltin o;
            return &o;
        }

        virtual Object *call(address_t num, Object **param) {
            //assert num == 1
            return (param[0]->__bool__() ? (Object*)TrueObject::alloc() : (Object*)FalseObject::alloc());
        }
};
// map, min, max, sum, pow, reversed, abs, any, all, chr, divmod

class RangeIterator: public Object {
    public:
        int cur, stop, step;
        RangeIterator(int start, int _stop, int _step): Object(OBJ_ITER), cur(start), stop(_stop), step(_step) { }
        virtual Object *__next__() {
            if ((step > 0 && cur < stop) || (step < 0 && cur > stop)) {
                Object *r=IntegerObject::alloc(cur);
                cur += step;
                return r;
            } else
                return NULL;
        }
};

class RangeBuiltin: public BuiltinObject {
    public:
        static BuiltinObject *alloc() {
            static RangeBuiltin o;
            return &o;
        }
        virtual Object *call(address_t num, Object **param) {
            int start, stop, step;
            if (num == 1) {
                stop = ((IntegerObject*)param[0])->val;
                start = 0;
                step = start <= stop ? 1 : -1;
            } else if (num == 2) {
                start = ((IntegerObject*)param[0])->val;
                stop = ((IntegerObject*)param[1])->val;
                step = start <= stop ? 1 : -1;
            } else {
                start = ((IntegerObject*)param[0])->val;
                stop = ((IntegerObject*)param[1])->val;
                step = ((IntegerObject*)param[2])->val;
                if ((start <= stop && step < 0) || (start >= stop && step > 0)) step *= -1;
            }
            return new RangeIterator(start, stop, step);
        }
};

void builtins_init() {
    builtins_dict["print"] = PrintBuiltin::alloc();
    builtins_dict["len"] = LenBuiltin::alloc();
    builtins_dict["bool"] = BoolBuiltin::alloc();
    builtins_dict["iter"] = IterBuiltin::alloc();
    builtins_dict["next"] = NextBuiltin::alloc();
    builtins_dict["range"] = RangeBuiltin::alloc();
}
