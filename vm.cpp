#include "vm.h"
#include <stdio.h>

#define TOP() (val_stack_p[-1])
#define POP() (*--val_stack_p)
#define PUSHy(v) (*val_stack_p++ = (v))

#define POPx() {\
    --val_stack_p;\
    val_stack_p[0]->decref();\
}

#define PUSH(v) {\
    (*val_stack_p) = (v);\
    (*val_stack_p)->incref();\
    ++val_stack_p;\
}


VM::VM(CompiledProgram &_p): p(_p) {
    var_stack_p = var_stack;
    val_stack_p = val_stack;
    bind_stack_p = NULL;
}

VM::~VM() {
}

void VM::read_opcode() {
    op.read(p.code);
}

void VM::set_pc(const address_t &addr) {
    p.code.set_offset(addr);
}

void VM::reset() {
    var_stack_p = var_stack;
    val_stack_p = val_stack;
    set_pc(0);
}

void VM::step() {
    Object *o1, *o2, *o3;
    address_t *aptr;
    ConstObject *co;
    read_opcode();
    switch (op.opcode) {
        case POP_TOP:
            POPx();
            break;
        case DUP_TOP:
            PUSH(TOP());
            break;
        case LOAD_NONE:
            PUSHy(NoneObject::alloc());
            break;
        case LOAD_TRUE:
            PUSHy(TrueObject::alloc());
            break;
        case LOAD_FALSE:
            PUSHy(FalseObject::alloc());
            break;
        case LOAD_SUBSCR:
            o1 = POP();
            o2 = POP();
            PUSH(o1->__getitem__(o2));
            o1->decref();
            o2->decref();
            break;
        case DELETE_SUBSCR:
            o1 = POP();
            o2 = POP();
            o2->__delitem__(o1);
            o1->decref();
            o2->decref();
            break;
        case STORE_SUBSCR:
            o1 = POP();
            o2 = POP();
            o3 = POP();
            o2->__setitem__(o1, o3);
            o1->decref();
            o2->decref();
            o3->decref();
            break;
        case COMPARE_LT:
            o1 = POP();
            o2 = POP();
            PUSH(o2->__lt__(o1));
            o1->decref();
            o2->decref();
            break;
        case COMPARE_LE:
            o1 = POP();
            o2 = POP();
            PUSH(o2->__le__(o1));
            o1->decref();
            o2->decref();
            break;
        case COMPARE_GT:
            o1 = POP();
            o2 = POP();
            PUSH(o2->__gt__(o1));
            o1->decref();
            o2->decref();
            break;
        case COMPARE_GE:
            o1 = POP();
            o2 = POP();
            PUSH(o2->__ge__(o1));
            o1->decref();
            o2->decref();
            break;
        case COMPARE_EQ:
            o1 = POP();
            o2 = POP();
            PUSH(o2->__eq__(o1));
            o1->decref();
            o2->decref();
            break;
        case COMPARE_NE:
            o1 = POP();
            o2 = POP();
            PUSH(o2->__ne__(o1));
            o1->decref();
            o2->decref();
            break;
        case COMPARE_IS:
            o1 = POP();
            o2 = POP();
            if (o2 == o1)
                PUSH(TrueObject::alloc())
            else
                PUSH(FalseObject::alloc())
            o1->decref();
            o2->decref();
            break;
        case COMPARE_IN:
            o1 = POP();
            o2 = POP();
            PUSHy(o2->__contains__(o1));
            o1->decref();
            o2->decref();
            break;
        case BINARY_ADD:
            o1 = POP();
            o2 = POP();
            PUSH(o2->__add__(o1));
            o1->decref();
            o2->decref();
            break;
        case BINARY_SUBTRACT:
            o1 = POP();
            o2 = POP();
            PUSH(o2->__sub__(o1));
            o1->decref();
            o2->decref();
            break;
        case BINARY_MULTIPLY:
            o1 = POP();
            o2 = POP();
            PUSH(o2->__mul__(o1));
            o1->decref();
            o2->decref();
            break;
        case BINARY_TRUE_DIVIDE:
            o1 = POP();
            o2 = POP();
            PUSH(o2->__truediv__(o1));
            o1->decref();
            o2->decref();
            break;
        case BINARY_FLOOR_DIVIDE:
            o1 = POP();
            o2 = POP();
            PUSH(o2->__floordiv__(o1));
            o1->decref();
            o2->decref();
            break;
        case BINARY_MODULO:
            o1 = POP();
            o2 = POP();
            PUSH(o2->__mod__(o1));
            o1->decref();
            o2->decref();
            break;
        case BINARY_POW:
            o1 = POP();
            o2 = POP();
            PUSH(o2->__pow__(o1));
            o1->decref();
            o2->decref();
            break;
        case BINARY_LSHIFT:
            o1 = POP();
            o2 = POP();
            PUSH(o2->__lshift__(o1));
            o1->decref();
            o2->decref();
            break;
        case BINARY_RSHIFT:
            o1 = POP();
            o2 = POP();
            PUSH(o2->__rshift__(o1));
            o1->decref();
            o2->decref();
            break;
        case BINARY_AND:
            o1 = POP();
            o2 = POP();
            PUSH(o2->__and__(o1));
            o1->decref();
            o2->decref();
            break;
        case BINARY_OR:
            o1 = POP();
            o2 = POP();
            PUSH(o2->__or__(o1));
            o1->decref();
            o2->decref();
            break;
        case BINARY_XOR:
            o1 = POP();
            o2 = POP();
            PUSH(o2->__xor__(o1));
            o1->decref();
            o2->decref();
            break;
        case UNARY_POSITIVE:
            TOP()->__pos__();
            break;
        case UNARY_NEGATIVE:
            TOP()->__neg__();
            break;
        case UNARY_INVERT:
            TOP()->__inv__();
            break;
        case UNARY_NOT:
            o1 = POP();
            if (o1->__bool__() == true)
                PUSHy(FalseObject::alloc());
            else
                PUSHy(TrueObject::alloc());
            o1->decref();
            break;
        case GET_ITER:
            o1 = POP();
            PUSH(o1->__iter__());
            o1->decref();
            break;
        case RETURN_VALUE:
            o1 = POP(); // return value
            o2 = POP(); // return address;
            PUSHy(o1);
            set_pc(((ReturnObject*)o2)->address);
            bind_stack_p = ((ReturnObject*)o2)->bind_stack;
            o2->decref();
            break;
        case LOAD_CONST:
            co = p.consts[op.oparg];
            switch(co->type) {
                case CO_REAL:
                    PUSH(RealObject::alloc(*((real_t*)co->val)));
                    break;
                case CO_INTEGER:
                    PUSH(IntegerObject::alloc(*((integer_t*)co->val)));
                    break;
                case CO_STRING:
                    PUSH(StringObject::alloc((char*)co->val));
                    break;
                case CO_FUNCTION:
                    aptr = (address_t*)co->val;
                    PUSH(FunctionObject::alloc(aptr[0], aptr[1], aptr[2], aptr[3]));
                    break;
                default:
                    break;
            }
            break;
        case LOAD_PARENT:
            PUSH(bind_stack_p[op.oparg]);
            break;
        case LOAD_BUILTIN:
            PUSH(p.builtins[op.oparg]);
            break;
        case LOAD_LOCAL:
            PUSH(var_stack_p[op.oparg]);
            break;
        case STORE_LOCAL:
            var_stack_p[op.oparg] = POP();
            break;
        case DELETE_LOCAL:
            var_stack_p[op.oparg]->decref();
            var_stack_p[op.oparg]=NULL;
            break;
        case CALL_FUNCTION:
            o1 = POP(); // function
            if (o1->type == OBJ_BUILTIN) {
                o2 = ((BuiltinObject*)o1)->call(op.oparg, &val_stack_p[-op.oparg]);
                for (int i=0; i<op.oparg; i++) {
                    POPx();//decref too
                }
                PUSH(o2);
            } else {
                if (((FunctionObject*)o1)->num_params != op.oparg) {
                    for (int i=0; i<op.oparg; i++) {
                        POPx();//decref too
                    }
                    PUSHy(NoneObject::alloc());
                } else {
                    var_stack_p = &var_stack_p[((FunctionObject*)o1)->num_vars];
                    for (int i=0; i<op.oparg; i++) {
                        var_stack_p[i] = POP();
                    }
                    PUSH(ReturnObject::alloc(p.code.get_offset(), bind_stack_p));
                    bind_stack_p = ((FunctionObject*)o1)->binds;
                    set_pc(((FunctionObject*)o1)->address);
                }
            }
            break;
        case BUILD_MAP:
            val_stack_p = &val_stack_p[-2*op.oparg];
            PUSH(DictObject::alloc(op.oparg, val_stack_p)); // should not do incref on elements
            break;
        case BUILD_LIST:
            val_stack_p = &val_stack_p[-op.oparg];
            PUSH(ListObject::alloc(op.oparg, val_stack_p)); // should not do incref on elements
            break;
        case BUILD_BINDING:
            o1 = POP();
            // assert o1->type == function o1->num_binds == oparg
            if (((FunctionObject*)o1)->num_binds != op.oparg) {
                for (int i=0; i<op.oparg; i++) {
                    POPx();
                }
            } else {
                for (int i=0; i<op.oparg; i++) {
                    ((FunctionObject*)o1)->binds[i] = POP();// do NOT do decref
                }
            }
            PUSHy(o1);
            break;
        case JUMP_ABSOLUTE:
            set_pc(op.oparg);
            break;
        case POP_JUMP_IF_TRUE:
            o1 = POP();
            if (o1->__bool__() == true)
                set_pc(op.oparg);
            o1->decref();
            break;
        case POP_JUMP_IF_FALSE:
            o1 = POP();
            if (o1->__bool__() == false)
                set_pc(op.oparg);
            o1->decref();
            break;
        case JUMP_IF_TRUE_OR_POP:
            o1 = TOP();
            if (o1->__bool__() == true)
                set_pc(op.oparg);
            else
                POPx();
            break;
        case JUMP_IF_FALSE_OR_POP:
            o1 = TOP();
            if (o1->__bool__() == false)
                set_pc(op.oparg);
            else
                POPx();
            break;
        case FOR_ITER:
            o1 = TOP();
            o2 = o1->__next__();
            if (o2 == NULL) {
                POPx();
                set_pc(op.oparg);
            }
            else
                PUSH(o2);
            break;
        default:
            break;
    }
}
