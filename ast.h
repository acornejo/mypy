#ifndef __AST_H__
#define __AST_H__

#include <vector>
#include <map>
#include <string>
#include <iostream>
#include "buffer.h"
#include "types.h"
#include "bytecode.h"
#include "objects.h"

namespace ast {
    namespace node_type {
        typedef enum {
            AST_NONE,
            AST_TRUE,
            AST_FALSE,
            AST_INTEGER,
            AST_REAL,
            AST_STRING,
            AST_VARIABLE,
            AST_BINARY,
            AST_UNARY,
            AST_AND,
            AST_OR,
            AST_PAIR,
            AST_PAIR_LIST,
            AST_IDENT_LIST,
            AST_DICT,
            AST_EXPR_LIST,
            AST_LIST,
            AST_SUBSCR,
            AST_CALL,
            AST_STMT_LIST,
            AST_EXPR_STMT,
            AST_ASSIGN,
            AST_DEL,
            AST_IF_ELSE,
            AST_FOR,
            AST_WHILE,
            AST_BREAK,
            AST_CONTINUE,
            AST_FUNCTION,
            AST_RETURN
        } node_type_t;
    }

    typedef enum {
        VAR_LOCAL,
        VAR_PARENT,
        VAR_BUILTIN
    } var_type_t;

    using namespace node_type;

    class Node;
    typedef std::vector<Node *> NodeList;

    class Node {
        public:
            node_type_t type;
            Node *parentNode;
            Node *nextSibling;
            Node *firstChild;
            Node *lastChild;
            size_t num_children;
            NodeList childNodes;

            Node(node_type_t _type=AST_STMT_LIST): type(_type), parentNode(NULL), nextSibling(NULL), firstChild(NULL), lastChild(NULL), num_children(0) { }

            virtual ~Node();
            void appendChild(Node *);
            void extend(Node *);

            virtual void compileTo(Buffer&);
    };

    class None: public Node {
        public:
            None(): Node(AST_NONE) { }
            void compileTo(Buffer&);
    };

    class True: public Node {
        public:
            True(): Node(AST_TRUE) { }
            void compileTo(Buffer&);
    };

    class False: public Node {
        public:
            False(): Node(AST_FALSE) { }
            void compileTo(Buffer&);
    };

    class Const: public Node {
        public:
            int idx;
            Const(node_type_t type): Node(type) { }
    };

    class Integer: public Const {
        public:
            integer_t val;
            Integer(integer_t _val): Const(AST_INTEGER), val(_val) { }
            void compileTo(Buffer&);
    };

    class Real: public Const {
        public:
            real_t val;
            Real(real_t _val): Const(AST_REAL), val(_val) { }
            void compileTo(Buffer&);
    };


    class String: public Const {
        public:
            std::string *val;
            String(std::string *_val): Const(AST_STRING), val(_val) { }
            virtual ~String() { delete val; }
            void compileTo(Buffer&);
    };

    class Variable: public Const {
        public:
            var_type_t vtype;
            bool write;
            std::string *val;
            Variable(std::string *_val): Const(AST_VARIABLE), vtype(VAR_LOCAL), write(false), val(_val) { }
            virtual ~Variable() { delete val; }
            void compileTo(Buffer&);
    };

    class Binary: public Node {
        public:
            int type;
            Node *left, *right;
            Binary(int _type, Node *_left, Node *_right): Node(AST_BINARY), type(_type), left(_left), right(_right) {
                appendChild(left);
                appendChild(right);
            }
            void compileTo(Buffer&);
    };

    class Unary: public Node {
        public:
            int type;
            Node *down;
            Unary(int _type, Node *_down): Node(AST_UNARY), type(_type), down(_down) {
                appendChild(down);
            }
            void compileTo(Buffer&);
    };

    class And: public Node {
        public:
            Node *left, *right;
            And(Node *_left, Node *_right): Node(AST_AND), left(_left), right(_right) {
                appendChild(left);
                appendChild(right);
            }
            void compileTo(Buffer&);
    };

    class Or: public Node {
        public:
            Node *left, *right;
            Or(Node *_left, Node *_right): Node(AST_OR), left(_left), right(_right) {
                appendChild(left);
                appendChild(right);
            }
            void compileTo(Buffer&);
    };


    class Pair: public Node {
        public:
            Node *left, *right;
            Pair(Node *_left, Node *_right): Node(AST_PAIR), left(_left), right(_right) {
                appendChild(left);
                appendChild(right);
            }
            void compileTo(Buffer&);
    };

    class PairList: public Node {
        public:
            PairList(): Node(AST_PAIR_LIST) { }
    };

    class Dict: public Node {
        public:
            Node *list;
            Dict(Node *_list): Node(AST_DICT), list(_list) {
                appendChild(list);
            }
            void compileTo(Buffer&);
    };

    class ExprList: public Node {
        public:
            ExprList(): Node(AST_EXPR_LIST) { }
    };

    class List: public Node {
        public:
            Node *list;
            List(Node *_list): Node(AST_LIST), list(_list) {
                appendChild(list);
            }
            void compileTo(Buffer&);
    };

    class Call: public Node {
        public:
            Node *parent, *params;
            Call(Node *_parent, Node *_params): Node(AST_CALL), parent(_parent), params(_params) {
                appendChild(parent);
                appendChild(params);
            }
            void compileTo(Buffer&);
    };

    class Subscript: public Node {
        public:
            Node *var, *idx;
            Subscript(Node *_var, Node *_idx): Node(AST_SUBSCR), var(_var), idx(_idx) {
                appendChild(var);
                appendChild(idx);
            }
            void compileTo(Buffer&);
    };

    class StmtList: public Node {
        public:
            StmtList(): Node(AST_STMT_LIST) { }
    };


    class ExprStatement: public Node {
        public:
            Node *e;
            ExprStatement(Node *_e): Node(AST_EXPR_STMT), e(_e) {
                appendChild(e);
            }
            void compileTo(Buffer&);
    };

    class Assign: public Node {
        public:
            Node *lval, *rval;
            int dups;
            Assign(Node *_lval, Node *_rval): Node(AST_ASSIGN), lval(_lval), rval(_rval), dups(0) {
                if (lval->type == AST_VARIABLE)
                    ((Variable*)lval)->write = true;
                if (rval->type == AST_ASSIGN)
                    ((Assign*)rval)->chain();
                appendChild(lval);
                appendChild(rval);
            }

            void chain() { if (rval->type == AST_ASSIGN) ((Assign*)rval)->chain(); else dups++; }
            void compileTo(Buffer&);
    };

    class Delete: public Node {
        public:
            Node *list;
            Delete(Node *_list): Node(AST_DEL), list(_list) {
                appendChild(list);
            }
            void compileTo(Buffer&);
    };

    class IfElse: public Node {
        public:
            Node *expr, *body, *ifelse;
            IfElse(Node *_expr, Node *_body, Node *_ifelse): Node(AST_IF_ELSE), expr(_expr), body(_body), ifelse(_ifelse) {
                appendChild(expr);
                appendChild(body);
                if (ifelse != NULL)
                    appendChild(ifelse);
            }
            void compileTo(Buffer&);
    };

    class Loop: public Node {
        public:
            address_t start_loop;
            std::vector<address_t> end_loop;
            Loop(node_type_t type): Node(type) { }
    };

    class For: public Loop {
        public:
            Node *var, *iterable, *body;
            For(Node *_var, Node *_iterable, Node *_body): Loop(AST_FOR), var(_var), iterable(_iterable), body(_body) {
                ((Variable*)var)->write = true;
                appendChild(var);
                appendChild(iterable);
                appendChild(body);
            }
            void compileTo(Buffer&);
    };

    class While: public Loop {
        public:
            Node *expr, *body;
            While(Node *_expr, Node *_body): Loop(AST_WHILE), expr(_expr), body(_body) {
                appendChild(expr);
                appendChild(body);
            }
            void compileTo(Buffer&);
    };

    class Break: public Node {
        public:
            Break(): Node(AST_BREAK) { }
            void compileTo(Buffer&);
    };

    class Continue: public Node {
        public:
            Continue(): Node(AST_CONTINUE) { }
            void compileTo(Buffer&);
    };

    class IdentList: public Node {
        public:
            IdentList(): Node(AST_IDENT_LIST) { }
            void compileTo(Buffer&);
    };

    class FunctionDef: public Const {
        public:
            std::string *name;
            Node binds;
            Node *params, *body;
            FunctionDef(std::string *_name, Node*_params, Node *_body): Const(AST_FUNCTION), name(_name), params(_params), body(_body) {
                appendChild(params);
                appendChild(body);
            }
            void compileTo(Buffer&);
    };

    class Return: public Node {
        public:
            Node *e;
            Return(Node *_e): Node(AST_RETURN), e(_e) {
                appendChild(e);
            }
            void compileTo(Buffer&);
    };
}

#endif//__AST_H__
