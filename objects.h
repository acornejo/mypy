#ifndef __OBJECTS_H__
#define __OBJECTS_H__

#include <vector>
#include <string>
#include <unordered_map>
#include <math.h>
#include "types.h"

typedef enum {
    OBJ_NONE,
    OBJ_INTEGER,
    OBJ_TRUE,
    OBJ_FALSE,
    OBJ_REAL,
    OBJ_STRING,
    OBJ_ITER,
    OBJ_LIST,
    OBJ_DICT,
    OBJ_RETURN,
    OBJ_FUNCTION,
    OBJ_BUILTIN,
} object_type_t;

class Object {
    public:
        uint8_t type;
        uint8_t ref;

        Object(object_type_t _type): type(_type), ref(0) { }
        virtual ~Object() { }

        // conversion
        virtual hash_t __hash__() const { return 0; }
        virtual std::string __str__() const { return ""; }
        virtual bool __bool__() const {return false; }

        // comparison
        virtual Object *__eq__(const Object *) const { return NULL; }
        virtual Object *__ne__(const Object *) const { return NULL; }
        virtual Object *__lt__(const Object *) const { return NULL; }
        virtual Object *__le__(const Object *) const { return NULL; }
        virtual Object *__gt__(const Object *) const { return NULL; }
        virtual Object *__ge__(const Object *) const { return NULL; }
        // numeric types
        virtual Object *__add__(const Object *) const { return NULL; }
        virtual Object *__sub__(const Object *) const { return NULL; }
        virtual Object *__mul__(const Object *) const { return NULL; }
        virtual Object *__truediv__(const Object *) const { return NULL; }
        virtual Object *__floordiv__(const Object *) const { return NULL; }
        virtual Object *__mod__(const Object *) const { return NULL; }
        virtual Object *__pow__(const Object *) const { return NULL; }
        // bit operations
        virtual Object *__lshift__(const Object *) const { return NULL; }
        virtual Object *__rshift__(const Object *) const { return NULL; }
        virtual Object *__and__(const Object *) const { return NULL; }
        virtual Object *__or__(const Object *) const { return NULL; }
        virtual Object *__xor__(const Object *) const{ return NULL; }
        // new round?
        // unary operations
        virtual void __neg__() { }
        virtual void __pos__() { }
        virtual void __abs__() { }
        virtual void __inv__() { }

        // container types
        virtual size_t __len__() const { return 0; }
        virtual Object * __contains__(const Object *key) const { return NULL; }
        virtual Object *__getitem__(const Object *key) const { return NULL; }
        virtual void __setitem__(const Object *key, Object *val) { }
        virtual void __delitem__(const Object *key) { }

        // iterator
        virtual Object *__iter__() { return this; }
        virtual Object *__next__() { return NULL; }

        void incref() {
            ref++;
        }

        void decref() {
            ref--;
            // if (ref == 0 && type != OBJ_BUILTIN)
            //     delete this;
        }
};

class NoneObject: public Object {
    public:
        virtual hash_t __hash__() const { return 0xdeadbeef; }
        virtual std::string __str__() const { return "None"; }
        virtual bool __bool__() const {return false; }

        static NoneObject *alloc() {
            static NoneObject f;
            return &f;
        }
    private:
        NoneObject(): Object(OBJ_FALSE) {}
        virtual ~NoneObject() {}
        NoneObject(const NoneObject &);
        NoneObject &operator=(const NoneObject &);
};

class IntegerObject: public Object {
    public:
        integer_t val;

        hash_t __hash__() const { return val; }
        std::string __str__() const { return std::to_string(val); } // fix
        bool __bool__() const { return val != 0; }

        static IntegerObject *alloc(integer_t val) {
            return new IntegerObject(val);
        }

        virtual Object *__eq__(const Object *o) const;
        virtual Object *__ne__(const Object *o) const;
        virtual Object *__lt__(const Object *o) const;
        virtual Object *__le__(const Object *o) const;
        virtual Object *__gt__(const Object *o) const;
        virtual Object *__ge__(const Object *o) const;

        virtual Object *__add__(const Object *) const;
        virtual Object *__sub__(const Object *) const;
        virtual Object *__mul__(const Object *) const;
        virtual Object *__truediv__(const Object *) const;
        virtual Object *__floordiv__(const Object *) const;
        virtual Object *__mod__(const Object *) const;
        virtual Object *__pow__(const Object *) const;
        // bit operations
        virtual Object *__lshift__(const Object *) const;
        virtual Object *__rshift__(const Object *) const;
        virtual Object *__and__(const Object *) const;
        virtual Object *__or__(const Object *) const;
        virtual Object *__xor__(const Object *) const;
        // unary operations
        virtual void __neg__();
        virtual void __pos__();
        virtual void __abs__();
        virtual void __inv__();
    protected:
        IntegerObject(integer_t _val=0): Object(OBJ_INTEGER), val(_val) { }
        ~IntegerObject() {}
        IntegerObject(const IntegerObject &);
        IntegerObject &operator=(const IntegerObject &);
};

class FalseObject: public IntegerObject {
    public:
        static FalseObject *alloc() {
            static FalseObject f; return &f;
        }

        virtual std::string __str__() const { return "False"; }
        virtual bool __bool__() const { return false; }

        virtual Object *__and__(const Object *) const;
        virtual Object *__or__(const Object *) const;
        virtual Object *__xor__(const Object *) const;

    private:
        FalseObject(): IntegerObject(0) { type = OBJ_FALSE; }
        virtual ~FalseObject() {}
        FalseObject(const FalseObject &);
        FalseObject &operator=(const FalseObject &);
};

class TrueObject: public IntegerObject {
    public:
        static TrueObject *alloc() {
            static TrueObject f;
            return &f;
        }

        virtual std::string __str__() const { return "True"; }
        virtual bool __bool__() const { return true; }

        virtual Object *__and__(const Object *) const;
        virtual Object *__or__(const Object *) const;
        virtual Object *__xor__(const Object *) const;

    private:
        TrueObject(): IntegerObject(1) { type = OBJ_FALSE; }
        virtual ~TrueObject() {}
        TrueObject(const TrueObject &);
        TrueObject &operator=(const TrueObject &);
};

class RealObject: public Object {
    public:
        real_t val;

        static RealObject *alloc(integer_t val) {
            return new RealObject(val);
        }

        virtual hash_t __hash__() const { return *((hash_t*)((void *)&val)); }
        virtual std::string __str__() const { return std::to_string(val); } // fix
        virtual bool __bool__() const { return val != 0; }

        virtual Object *__eq__(const Object *o) const;
        virtual Object *__ne__(const Object *o) const;
        virtual Object *__lt__(const Object *o) const;
        virtual Object *__le__(const Object *o) const;
        virtual Object *__gt__(const Object *o) const;
        virtual Object *__ge__(const Object *o) const;

        virtual Object *__add__(const Object *o) const { return RealObject::alloc(val + ((RealObject*)o)->val); }
        virtual Object *__sub__(const Object *o) const { return RealObject::alloc(val - ((RealObject*)o)->val); }
        virtual Object *__mul__(const Object *o) const { return RealObject::alloc(val * ((RealObject*)o)->val); }
        virtual Object *__truediv__(const Object *o) const { return RealObject::alloc(val / ((RealObject*)o)->val); }
        virtual Object *__floordiv__(const Object *o) const { return RealObject::alloc(floor(val / ((RealObject*)o)->val)); }
        virtual Object *__mod__(const Object *o) const { return RealObject::alloc(fmod(val, ((RealObject*)o)->val)); }
        virtual Object *__pow__(const Object *o) const { return RealObject::alloc(pow(val, ((RealObject*)o)->val)); } // fix

        // unary operations
        virtual void __neg__() { val = -val; }
        virtual void __pos__() { }
        virtual void __abs__() { val = val < 0 ? -val : val; }
    private:
        RealObject(integer_t _val=0): Object(OBJ_REAL), val(_val) { }
        ~RealObject() {}
        RealObject(const RealObject &);
        RealObject &operator=(const RealObject &);
};

class StringObject: public Object {
    public:
        const char *val;
        size_t size;
        bool manage;
        hash_t hash;

        static StringObject *alloc(const char *val, const bool &manage=false) {
            return new StringObject(val, manage);
        }

        virtual hash_t __hash__() const;
        virtual std::string __str__() const { return val; }
        virtual bool __bool__() const { return size != 0; }

        virtual Object *__eq__(const Object *) const;
        virtual Object *__ne__(const Object *) const;
        virtual Object *__lt__(const Object *) const;
        virtual Object *__le__(const Object *) const;
        virtual Object *__gt__(const Object *) const;
        virtual Object *__ge__(const Object *) const;

        virtual size_t __len__() const { return size; }
        virtual Object *__contains__(const Object *key) const;
        virtual Object *__getitem__(const Object *key) const;

    private:
        StringObject(const char *_val=0, const bool &_manage=false): Object(OBJ_STRING), val(_val), manage(_manage), hash(0) { }
        ~StringObject() { if (manage) delete []val; }
        StringObject(const StringObject &);
        StringObject &operator=(const StringObject &);
};

class ListObject: public Object {
    public:
        std::vector<Object *> list;

        static ListObject *alloc(address_t num, Object **params) {
            return new ListObject(num, params);
        }

        virtual size_t __len__() const { return list.size(); }
        virtual Object *__contains__(const Object *key) const {
            for (size_t i =0; i<list.size(); i++)
                if (list[i]->__eq__(key)->__bool__())
                    return TrueObject::alloc();
            return FalseObject::alloc();
        }
        virtual Object *__getitem__(const Object *key) const {
            int i = ((IntegerObject*)key)->val;
            // assert i in right range
            return list[i];
        }
        virtual void __setitem__(const Object *key, Object *val) {
            int i = ((IntegerObject*)key)->val;
            // assert i in right range
            list[i]->decref();
            list[i] = val;
            val->incref();
        }
        virtual void __delitem(const Object *key) {
            int i = ((IntegerObject*)key)->val;
            list[i]->decref();
            list.erase(list.begin()+i);
        }
        virtual Object *__iter__();

    private:
        ListObject(address_t num, Object **params): Object(OBJ_LIST), list(num) {
            for (int i=0; i<num; i++)
                list[i] = params[i];
        }
        virtual ~ListObject() {
            for (size_t i=0; i<list.size(); i++)
                list[i]->decref();
            list.clear();
        }
        ListObject(const ListObject &);
        ListObject &operator=(const ListObject &);
};

class ListIterator: public Object {
    public:
        std::vector<Object*>::iterator i;
        Object *list;
        ListIterator(Object *_list): Object(OBJ_ITER), list(_list) {
            list->incref();
            i = ((ListObject*)list)->list.begin();
        }
        virtual ~ListIterator() {
            list->decref();
        }
        virtual Object *__next__() {
            if (i == ((ListObject*)list)->list.end())
                return NULL;
            return *i++;
        }
};

struct ObjectHash {
    hash_t operator()(const Object *o) const { return o->__hash__(); }
};
struct ObjectEqual {
    bool operator()(const Object *o1, const Object *o2) const { return o1->__eq__(o2)->__bool__(); }
};

typedef std::unordered_map<const Object*, Object*, ObjectHash, ObjectEqual> hmap_t;

class DictObject: public Object {
    public:
        hmap_t hash;
        static DictObject *alloc(address_t num, Object **params) {
            return new DictObject(num, params);
        }
        virtual size_t __len__() const { return hash.size(); }
        virtual Object *__contains__(const Object *key) const {
            if (hash.find(key) != hash.end())
                return TrueObject::alloc();
            else
                return FalseObject::alloc();
        }
        virtual Object *__getitem__(const Object *key) const {
            hmap_t::const_iterator i = hash.find(key);
            if (i == hash.end())
                return NULL;
            else
                return i->second;
        }
        virtual void __setitem__(const Object *key, Object *val) {
            std::pair<hmap_t::iterator,bool> i = hash.insert(hmap_t::value_type(key,val));
            if (i.second == true) {
                val->incref();
            } else {
                i.first->second->decref();
                i.first->second = val;
            }
            val->incref();
        }
        virtual void __delitem__(const Object *key) {
            hmap_t::const_iterator i = hash.find(key);
            if (i != hash.end()) {
                const_cast<Object*>(i->first)->decref();
                const_cast<Object*>(i->second)->decref();
                hash.erase(i);
            }
        }
        virtual Object *__iter__();

    private:
        DictObject(address_t num, Object **params): Object(OBJ_DICT), hash(num) {
            for (int i=0; i<num; i++)
                hash[params[i*2]] = params[i*2+1];
        }
        virtual ~DictObject() {
            hmap_t::iterator i;
            for (i=hash.begin(); i != hash.end(); ++i) {
                const_cast<Object*>(i->first)->decref();
                const_cast<Object*>(i->second)->decref();
            }
            hash.clear();
        }
        DictObject(const DictObject &);
        DictObject &operator=(const DictObject &);
};

class DictIterator: public Object {
    public:
        hmap_t::iterator i;
        Object *list;
        DictIterator(Object *_list): Object(OBJ_ITER), list(_list) {
            i = ((DictObject*)list)->hash.begin();
            list->incref();
        }
        virtual ~DictIterator() {
            list->decref();
        }
        virtual Object *__next__() {
            if (i == ((DictObject*)list)->hash.end())
                return NULL;
            return const_cast<Object*>((i++)->first);
        }
};

class FunctionObject: public Object {
    public:
        address_t address;
        address_t num_params;
        address_t num_vars;
        address_t num_binds;
        Object **binds;

        static Object *alloc(address_t a, address_t p, address_t v, address_t b) {
            return new FunctionObject(a,p,v,b);
        }

    private:
        FunctionObject(address_t _address, address_t _num_params, address_t _num_vars, address_t _num_binds): Object(OBJ_FUNCTION), address(_address), num_params(_num_params), num_vars(_num_vars), num_binds(_num_binds) {
            if (num_binds > 0)
                binds = new Object*[num_binds];
            else
                binds = NULL;
        }
        virtual ~FunctionObject() {
            if (num_binds > 0) {
                for (int i=0; i<num_binds; i++)
                    binds[i]->decref();
                delete []binds;
            }
        }
};

class ReturnObject: public Object {
    public:
        address_t address;
        Object **bind_stack;

        static Object *alloc(address_t a, Object **b) {
            return new ReturnObject(a, b);
        }
    private:
        ReturnObject(address_t _address, Object **_bind_stack): Object(OBJ_RETURN), address(_address), bind_stack(_bind_stack) { }
        ~ReturnObject() {}
};

class BuiltinObject: public Object {
    public:
        BuiltinObject(): Object(OBJ_BUILTIN) { }
        virtual Object *call(address_t num, Object **param) = 0;
};

#endif//__OBJECTS_H__
