#ifndef __BUFFER_H__
#define __BUFFER_H__

#include <string.h>
#include "types.h"

class Buffer {
    private:
        unsigned char *data;
        size_t capacity;
        address_t offset;
        size_t size;

    public:
        Buffer(): offset(0), size(0) {
            capacity = 1024;
            data = new unsigned char[capacity];
        }

        virtual ~Buffer() {
            delete []data;
        }

        void write(const void *d, size_t len) {
            if (capacity < offset + len) {
                unsigned char *newdata = new unsigned char[capacity*2];
                memcpy(newdata, data, capacity);
                delete []data;
                data = newdata;
                capacity = capacity*2;
            }
            memcpy(data + offset, d, len);
            offset += len;
            size = offset;
        }

        void read(void *d, size_t len) {
            memcpy(d, data + offset, len);
            offset += len;
        }

        void write(const void *d, size_t len, address_t off) {
            memcpy(data + off, d, len);
        }

        bool iseof() const {
            return offset >= size;
        }

        size_t get_size() const {
            return size;
        }

        address_t get_offset() const {
            return offset;
        }

        void set_offset(address_t _offset) {
            offset = _offset;
        }
};

#endif//__BUFFER_H__
