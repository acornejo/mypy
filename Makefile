TARGET = mypy
all: $(TARGET)

PARSER_LEXER = parser.cpp lexer.cpp
CPP_FILES = $(filter-out $(PARSER_LEXER), $(wildcard *.cpp)) $(PARSER_LEXER)
OBJ_FILES = $(CPP_FILES:.cpp=.o)

CXX = g++
CXXFLAGS = -O0 -Wall -g

LEX = flex
YACC = bison

ast.cpp: parser.hpp lexer.h

lexer.cpp lexer.h: mypy.l parser.hpp
	$(LEX) -o lexer.cpp --header-file=lexer.h mypy.l

parser.cpp parser.hpp: mypy.y
	$(YACC) -d -o parser.cpp mypy.y

$(TARGET): $(OBJ_FILES) parser.hpp
	$(CXX) $(CXX_FLAGS) -o $@ $(OBJ_FILES)

small: $(TARGET)
	strip -x $(TARGET)
	upx $(TARGET)

clean:
	rm parser.hpp parser.cpp lexer.cpp lexer.h $(TARGET) $(OBJ_FILES)
